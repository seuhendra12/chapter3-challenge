function filterCarByAvailability(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  // Mencetak data sebelum di filter
  console.log("Data sebelum di filter :");
  console.table(cars);

  // Tempat penampungan hasil
  const result = [];

  // Tulis code-mu disini
  // melakukan perulangan sepanjang array

  // Inisialisasi variabel panjang dengan cars.length
  const panjang = cars.length;

  // Mencetak ukuran panjang array
  console.log("Panjang Array = ", panjang);

  // Melakukan perulangan dengan for loop
  for (let i = 0; i < panjang - 1; i++) {

    //jika atribut pada cars ke-i itu available, maka push kedalam result
    if (cars[i].available === true) {

      console.log("Cars ke - " + i + " Available , PUSH!");

      result.push(cars[i]);

    } else {

      console.log("Cars ke - " + i + " Tidak available, tidak di PUSH");

    }
  }

  // Rubah code ini dengan array hasil filter berdasarkan availablity
  // Mencetak data sesudah di filter
  console.log("Data sesudah di filter : ");
  console.table(result);  
  return result;
}
