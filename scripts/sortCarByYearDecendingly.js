function sortCarByYearDescendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  // Mencetak data sebelum terurut secara descending
  console.log("Data sebelum terurut : ")
  console.table(cars);

  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  // Side Effects adalah efek samping setiap perubahan keadaan yang terjadi di luar function yang 
  // dipanggil. Tujuan terbesar dari setiap functional programming language adalah meminimalkan 
  // efek samping, dengan memisahkannya dari kode perangkat lunak lainnya.

  // Inisialisasi array result
  const result = [...cars];

  // Tulis code-mu disini
  // Inisialisasi variabel panjang dengan cars.length
  const panjang = cars.length;

  // Mencetak ukuran panjang array
  console.log("Panjang Array = ", panjang);

  // For loop
  for (let i = 0; i < panjang - 1; i++){
    
    // Looping perbandingan index
    for (let j = 0; j < panjang - 1; j++) {

      // Kondisi
      if (result[j].year < result[j + 1].year) {

        tmp = result[j];

        result[j] = result[j + 1];

        result[j + 1] = tmp;
        
      }

    }
    
  }

  // Rubah code ini dengan array hasil sorting secara descending
  // Mencetak data setelah terurut secara descending
  console.log("Data sesudah terurut :");
  console.table(result);
  return result;
}
